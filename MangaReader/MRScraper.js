var cheerio = require("cheerio"),
    configs = require("./configs"),
    Q       = require("q"),
    request = require("request");

var MRScraper = function(){
var self = this;

/**
 * Retrieve chapter url for the given manga from mangareader webpage
 */
self.GetMangaChapters = function(name){
	var deffered = Q.defer();
	request({
		url:configs.MRUrl+"/"+name,
		setTimeout:3000
	}, function(err,response,body){
			if(err){
				deffered.reject(new Error(err));
			}
			else {
				var $ = cheerio.load(body.toString());
				var ChapterList = [];
				
				$("#chapterlist table td").each(function(){
                    var chapStartUrl = $(this).find("a").attr("href");
					 if(chapStartUrl!=undefined){
						  
						var chapname     = $(this).find("a").text(),
						    chapSplit    = chapname.split(" "),
						    chapnr       = chapname.replace(/^\D+/g,'');
	                      
						var chapInfo = {
	                      StartUrl: configs.MRUrl+chapStartUrl,
	                      Title: chapname,
	                      ChapNumber: chapnr
						};

						ChapterList.push(chapInfo);
				    }

				});

				deffered.resolve(ChapterList);
			}

	});

  return deffered.promise;
}


};

module.exports = MRScraper;