/**
 * Created by aliyhuss on 12/16/14.
 */
var MangaScraper = require("../MangaScraper");
var should = require("should");
var sinon = require("sinon");
var fs = require("fs");
var Q = require("q");


var mangaModelFake = function () {
    var deffered = Q.defer();
    fs.readFile("test/data/Naruto.html", function (err, data) {
        if (err) {
            deffered.reject(err);

        } else {

            deffered.resolve(data.toString());

        }


    })

    return deffered.promise;
};



var searchModelFake = function () {
    var deffered = Q.defer();
    fs.readFile("test/data/Search.html", function (err, data) {
        if (err) {
            deffered.reject(err);

        } else {

            deffered.resolve(data.toString());

        }


    })

    return deffered.promise;
};


var searchPaginModelFake = function () {
    var deffered = Q.defer();
    fs.readFile("test/data/SearchPaging.html", function (err, data) {
        if (err) {
            deffered.reject(err);

        } else {

            deffered.resolve(data.toString());

        }


    })

    return deffered.promise;
};


describe("MangaScraper Test", function () {
    describe("#RequestContent()", function () {
        it("Should return error if url not provided", function (done) {
            MangaScraper.RequestContent().then(function (result) {
                done(new Error());
            }, function (err) {
                done();
            })
        })

    });

    describe("#GetMangaByID()", function () {

        it("Should fail when mangaId not provided", function (done) {
            MangaScraper.GetMangaByID().then(function (mangaModel) {
                done(new Error());
            }, function (err) {
                done();
            })
        })

        it("Should create manga model",function(done){

            var stub = sinon.stub(MangaScraper,"RequestContent",mangaModelFake);
            MangaScraper.GetMangaByID(15).then(function(model){
               // console.log(model);
                stub.restore();
                done();
            },function(err){
                done(err);
            })
        })

        it("Should create manga model with advanced object",function(done){

            var stub = sinon.stub(MangaScraper,"RequestContent",mangaModelFake);
            MangaScraper.GetMangaByID(10,{advanced:false}).then(function(model){
                console.log(model);
                stub.restore();
                done();
            },function(err){
                done(err);
            })
        })


    })

    describe("#SearchManga()",function(){
        it("Should fail when manga title not provided",function(done){
            MangaScraper.SearchManga().then(function(searchresults){
                done(new Error());
            },function(err){
                done();
            })
        })

        it("Should create search model successfully",function(done){

            var stub = sinon.stub(MangaScraper,"RequestContent",searchModelFake);
            MangaScraper.SearchManga("Naruto").then(function(searchResults){
                console.log(searchResults);
                stub.restore();
                done();
            },function(err){
                done(err);
            })
        })

        it("Should create search model successfully with pagination option",function(done){
            var stub = sinon.stub(MangaScraper,"RequestContent",searchPaginModelFake);
            MangaScraper.SearchManga("Naruto",{page:3}).then(function(searchResults){

                var currentpage = searchResults.Pagination.current;
                should(currentpage).be.equal(3);
                stub.restore();
                done();
            },function(err){
                done(err);
            })
        })
    })


});
